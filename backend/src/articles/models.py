from django.db import models


class Article(models.Model):
    title = models.CharField(max_length=120)
    content = models.TextField()

    def __str__(self):
        return self.title
class Artist(models.Model):
    name = models.CharField(max_length=50)
    age = models.PositiveSmallIntegerField()

    def __str__(self):
        return self.name

class Album(models.Model):

    title = models.CharField(max_length=50)
    artist = models.ForeignKey('Artist',on_delete=models.CASCADE,null=True)
    def __str__(self):
        return self.title

class Song(models.Model):

    name = models.CharField(max_length=50)
    artist = models.ForeignKey(Artist,on_delete=models.CASCADE,null=True)
    album = models.ForeignKey(Album,on_delete=models.CASCADE,null=True)
    def __str__(self):
        return self.name

