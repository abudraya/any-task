from django.contrib import admin

from .models import Article, Artist, Album, Song

admin.site.register(Article)
admin.site.register(Artist)
admin.site.register(Album)
admin.site.register(Song)
