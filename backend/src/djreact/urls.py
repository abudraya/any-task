from django.contrib import admin
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt
from django.conf.urls import url, include
from .schema import schema

urlpatterns = [
    url('api-auth/', include('rest_framework.urls')),
    url('admin/', admin.site.urls),
    url('api/', include('articles.api.urls')),
    url('graphql',csrf_exempt(GraphQLView.as_view(
        graphiql = True,
        schema=schema
    )))
]
