import graphene
from articles.schema import Query as articles_query

class Query(articles_query):
    pass

schema = graphene.Schema(query=Query)